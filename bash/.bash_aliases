alias ls='ls --color=auto'
alias la='ls -A'
alias ll='ls -lFh'
alias l='ls -CF'
alias lla='ls -alh'
alias dir='dir --color=auto'
alias diffd='diff -s -t -E --tabsize 2 --suppress-common-lines'
alias ff=freight-forwarder
alias vdir='vdir --color=auto'
alias vi='vim'
alias gg='git lg'
alias gis='git status'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'
alias c=clear
alias dps='docker ps'
alias dpsa='docker ps -a'
alias du='du -h'
alias df='df -h'
alias xterm='xterm -rv'
alias irb='docker run -it --rm -v $(pwd):/data dockerfile/ruby irb'
alias emacs='emacs -nw'
alias dm=docker-machine
alias dc=docker-compose
alias rc=rancher-compose

